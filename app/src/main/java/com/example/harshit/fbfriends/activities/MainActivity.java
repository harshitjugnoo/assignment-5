package com.example.harshit.fbfriends.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.example.harshit.fbfriends.R;
import com.example.harshit.fbfriends.util.AppConstants;
import com.example.harshit.fbfriends.util.FacebookUtils;
import com.facebook.Session;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.example.harshit.fbfriends.util.FacebookUtils.fbAccessToken;

public class MainActivity extends Activity implements AppConstants, View.OnClickListener {

    TextView textView;
    ListView listView;
    Button logoutButton;

    ArrayAdapter adapter;
    List<String> friendList = new ArrayList<>(); //Stores friends in an Array List
    List<String> friendId = new ArrayList<>(); //Stores their IDs in another Array List

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView = (TextView) findViewById(R.id.heading);
        listView = (ListView) findViewById(R.id.list);
        logoutButton = (Button) findViewById(R.id.btnLogOut);
        logoutButton.setOnClickListener(this);
        FacebookUtils.fbLogin(0, MainActivity.this); //Displays a dialog for Facebook Login

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() { //This displays the details of a friend on clicking the name of the friend
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String idSelected = friendId.get(position);
                String friendUri = URI_PROFILE + idSelected + ACCESS_TOKEN_SUBSTRING + fbAccessToken;
                final AsyncHttpClient client = new AsyncHttpClient();
                client.get(MainActivity.this, friendUri, new AsyncHttpResponseHandler() {
                            @Override
                            public void onSuccess(String s) {

                                try {
                                    JSONObject obj = new JSONObject(s);

                                    String firstName = obj.getString("first_name");
                                    String lastName = obj.getString("last_name");
                                    String gender = obj.getString("gender");
                                    String link = obj.getString("link");

                                    Intent intent = new Intent(MainActivity.this, DetailsActivity.class);
                                    intent.putExtra("firstName", firstName);
                                    intent.putExtra("lastName", lastName);
                                    intent.putExtra("gender", gender);
                                    intent.putExtra("link", link);
                                    startActivity(intent);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onFailure(Throwable throwable, String s) {
                                super.onFailure(throwable, s);
                            }
                        }
                );
            }
        });
    }

    public void getFriends(String fbAccessToken) { //This function receives the JSON data, parses it and displays it in list view

        String friends = URI + fbAccessToken;
        final AsyncHttpClient client = new AsyncHttpClient();
        client.get(MainActivity.this, friends, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int i, String s) {
                friendList.clear();
                try {
                    JSONObject allFriends = new JSONObject(s);
                    JSONArray friends = allFriends.getJSONArray("data");
                    for (int j = 0; j < friends.length(); j++) {
                        JSONObject friend = friends.getJSONObject(j);
                        String name = friend.getString("name");
                        String id = friend.getString("id");
                        friendList.add(name);
                        friendId.add(id);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                adapter = new ArrayAdapter(MainActivity.this, android.R.layout.simple_list_item_1, friendList);
                listView.setAdapter(adapter);
            }

            @Override
            public void onFailure(Throwable throwable, String s) {
                super.onFailure(throwable, s);
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Session.getActiveSession().onActivityResult(MainActivity.this, requestCode, resultCode, data);
    }

    @Override
    public void onClick(View v) { //This function is meant for logging out the current user
        Session.getActiveSession().closeAndClearTokenInformation(); //Clears the Access token
        friendList.clear(); //Clears the list of friends
        adapter.clear();
        FacebookUtils.fbLogin(0, MainActivity.this); //Displays the dialog again if the user wants to do another login

    }
}







