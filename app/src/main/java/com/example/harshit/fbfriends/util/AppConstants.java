package com.example.harshit.fbfriends.util;

/**
 * Created by Harshit on 1/30/2015.
 */
public interface AppConstants {
    int SUCCESS_CODE = 200;
    String URI = "https://graph.facebook.com/me/friends?access_token=";
    String URI_PROFILE = "https://graph.facebook.com/";
    String ACCESS_TOKEN_SUBSTRING = "?access_token=";
}
