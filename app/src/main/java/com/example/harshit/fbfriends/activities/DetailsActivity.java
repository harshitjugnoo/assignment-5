package com.example.harshit.fbfriends.activities;

import android.app.Activity;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.widget.TextView;
import com.example.harshit.fbfriends.R;

public class DetailsActivity extends Activity {
    TextView firstName, lastName, gender, link;
    Bundle extras;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.details_friend);
        firstName = (TextView) findViewById(R.id.firstName);
        lastName = (TextView) findViewById(R.id.lastName);
        gender = (TextView) findViewById(R.id.gender);
        link = (TextView) findViewById(R.id.link);
        extras = getIntent().getExtras();
        if (extras != null) {
            String profile_link = extras.getString("link");
            String html = "<a href=\"" + profile_link + "\">Click Here</a>";

            firstName.setText(extras.getString("firstName"));
            lastName.setText(extras.getString("lastName"));
            gender.setText(extras.getString("gender"));

            link.setMovementMethod(LinkMovementMethod.getInstance());
            link.setText(Html.fromHtml(html));
        }


    }
}
